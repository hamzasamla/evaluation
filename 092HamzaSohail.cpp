#include<iostream>
#include<stdlib.h>
#include<string.h>

using namespace std;

bool isOperator(char c)
{
    if(c =='+' || c =='-' || c =='*' || c =='/' || c=='(' || c=='<' || c=='>' || c=='|' || c=='&' || c=='=' || c=='!' )
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

bool isOperand(char c)
{
    if(c>='a' && c<='z')
    {
        return 1;
    }

    if(c>='A' && c<='Z')
    {
        return 1;
    }
    else
    {
    	return 0;
	}
}

int push(int *arr[],int index, int val)
{
	*arr[index]=val;
	index=index+1;
	return index;
}

int pop(int *arr[],int index)
{
	int temp=*arr[index];
	index--;
	return temp;
	
}

void evaluate(char postfix[],int length)
{
	
	int *eval=new int[length];
	int index=0;
	int a = 6, b = 2, c = 3, d = 4, e = 1, f = 0;
	
	cout<<"The Postfix Expression is: "<<endl;
	for(int i=0;i<length;i++)
	{
		cout<<postfix[i]<<" ";
		
		if(isOperand(postfix[i])==1)
	{
		
		if(postfix[i]=='a')
		{
		eval[index]=a;
		index++;
		}
		else if(postfix[i]=='b')
		{
		eval[index]=b;
		index++;
		}
		else if(postfix[i]=='c')
		{
		eval[index]=c;
		index++;
		}
		else if(postfix[i]=='d')
		{
		eval[index]=d;
		index++;
		}
		else if(postfix[i]=='e')
		{
		eval[index]=e;
		index++;
		}
		else if(postfix[i]=='f')
		{
		eval[index]=f;
		index++;
		}
		else
		{
			cout<<"NOT defined"<<endl;
		}
	}
	
	else if(isOperator(postfix[i])==1)
	{
		int second=eval[index-2];
		int first=eval[index-1];
		if(postfix[i]=='*')
		{
			eval[index-2]=second*first;
			index--;
		}
		else if(postfix[i]=='/')
		{
			eval[index-2]=second/first;
			index--;
		}
		
		else if(postfix[i]=='+')
		{
			eval[index-2]=second+first;
			index--;
		}
		else if(postfix[i]=='-')
		{
			eval[index-2]=second-first;
			index--;
		}
		else if(postfix[i]=='<')
		{
			(second<first)?eval[index-2]=1:eval[index-2]=0;
		}
		else if(postfix[i]=='>')
		{
			(second>first)?eval[index-2]=1:eval[index-2]=0;
			index--;
		}
		else if(postfix[i]=='&')
		{
			(second&&first)?eval[index-2]=1:eval[index-2]=0;
			index--;
		}
		else if(postfix[i]=='|')
		{
			(second||first)?eval[index-2]=1:eval[index-2]=0;
			index--;
		}
		else if(postfix[i]=='=')
		{
			(second==first)?eval[index-2]=1:eval[index-2]=0;
			index--;
		}
		else if(postfix[i]=='!')
		{
			(second!=first)?eval[index-2]=1:eval[index-2]=0;
			index--;
		}
	}
	
	}
	
	cout<<"\nThe Evaluated Answer is: "<<eval[0]<<endl;
}

int main()
{
	char postfix[]="abc*d/+e-";
	int length=strlen(postfix);
	evaluate(postfix,length);
}
